/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:27:43 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/09 10:31:17 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	wrong_arg(void)
{
	printf("Wrong number of Arguments\n./philosophers ");
	printf("[number_of_philosophers] [time_to_die] [time_to_eat] ");
	printf("[time_to_sleep] [number_of_times_each_philosophers_must_eat]\n");
}

int	error_type(char **argv)
{
	if (!str_digit(argv[1]) || !str_digit(argv[2]) || !str_digit(argv[3])
		|| !str_digit(argv[4]))
	{
		printf("Wrong type of argument.\n");
		printf("[+INT] [+INT] [+INT] ");
		printf("[+INT] [+INT]\n");
		return (1);
	}
	return (0);
}

static int	limitations(t_param *param)
{
	if (param->numb < 1 || param->t_die < 10 || param->t_eat < 10
		|| param->t_sleep < 10 || (param->eat_limit == 1 && param->nb_eat < 1))
		return (1);
	if (param->numb > INT_MAX || param->t_die > INT_MAX
		|| param->t_eat > INT_MAX || param->t_sleep > INT_MAX
		|| (param->eat_limit == 1 && param->nb_eat > INT_MAX))
		return (1);
	return (0);
}

int	error_value(t_param *param)
{
	if (limitations(param))
	{
		printf("Wrong value of argument.\nPlease enter :\n\t./philo\t\t");
		printf("number_of_philosophers > 0\n\t\t\ttime_to_die > 10\n\t\t\t");
		printf("time_to_eat > 10\n\t\t\ttime_to_sleep > 10\n\t\t\t");
		printf("number_of_time_a_philosopher_must_eat > 1\n");
		return (1);
	}
	return (0);
}
