/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alive_check.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:24:33 by alangloi          #+#    #+#             */
/*   Updated: 2021/10/08 16:28:50 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	must_eat(t_philo *philo)
{
	t_philo	*cur;

	cur = philo;
	while (cur != NULL)
	{
		if (cur->meals < philo->param->nb_eat)
			return ;
		cur = cur->next;
	}
	pthread_mutex_lock(&philo->param->_has_eat);
	philo->param->has_eat = 1;
	pthread_mutex_unlock(&philo->param->_has_eat);
}

void	is_dead(t_philo *philo)
{
	t_philo	*cur;

	cur = philo;
	while (cur != NULL)
	{
		if (check_last_meal(cur))
		{
			writing("died", philo);
			pthread_mutex_lock(&philo->param->_is_dead);
			philo->param->is_dead = 1;
			pthread_mutex_unlock(&philo->param->_is_dead);
			usleep(1000);
			break ;
		}
		cur = cur->next;
	}
}

void	*alive(void *arg)
{
	t_philo	*philo;

	philo = (t_philo *)arg;
	while (check_eat(philo) && check_dead(philo))
	{
		usleep(500);
		is_dead(philo);
		if (philo->param->eat_limit)
			must_eat(philo);
	}
	return (NULL);
}
