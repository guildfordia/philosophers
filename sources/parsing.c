/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:25:35 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/09 10:25:37 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

static t_param	*parse_param(char **argv)
{
	t_param	*param;

	param = malloc(sizeof(t_param));
	if (!param)
		return (NULL);
	param->has_eat = 0;
	param->is_dead = 0;
	param->eat_limit = 1;
	param->numb = ft_atoi(argv[1]);
	param->t_die = ft_atol(argv[2]);
	param->t_eat = ft_atol(argv[3]);
	param->t_sleep = ft_atol(argv[4]);
	if (argv[5])
		param->nb_eat = ft_atoi(argv[5]);
	else
		param->eat_limit = 0;
	param->orig_time = get_time();
	return (param);
}

t_param	*parsing(char **argv)
{
	t_param	*param;

	if (error_type(argv))
		exit(1);
	param = parse_param(argv);
	if (error_value(param))
		exit(1);
	if (!create_fork(param))
		return (0);
	return (param);
}
