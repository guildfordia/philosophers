/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tasks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:28:10 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/30 14:04:50 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

void	philo_eat(t_philo *philo)
{
	pthread_mutex_lock(philo->left);
	writing("has taken a fork", philo);
	pthread_mutex_lock(philo->right);
	writing("has taken a fork", philo);
	if (check_eat(philo) && check_dead(philo))
	{
		pthread_mutex_lock(&philo->_last_meal);
		philo->last_meal = get_time();
		pthread_mutex_unlock(&philo->_last_meal);
		writing("is eating", philo);
		wait_until(philo->param->t_eat);
		pthread_mutex_unlock(philo->left);
		pthread_mutex_unlock(philo->right);
		philo->meals++;
	}
	else
	{
		pthread_mutex_unlock(philo->left);
		pthread_mutex_unlock(philo->right);
	}
}

void	philo_sleep(t_philo *philo)
{
	if (check_eat(philo) && check_dead(philo))
	{
		writing("is sleeping", philo);
		wait_until(philo->param->t_sleep);
	}
}

void	philo_think(t_philo *philo)
{
	if (check_eat(philo) && check_dead(philo))
		writing("is thinking", philo);
}
