/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 18:14:39 by alangloi          #+#    #+#             */
/*   Updated: 2021/10/05 19:01:25 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

int	check_last_meal(t_philo *philo)
{
	pthread_mutex_lock(&philo->_last_meal);
	if ((get_time() - philo->last_meal) * 1000 >= philo->param->t_die * 1000)
	{
		pthread_mutex_unlock(&philo->_last_meal);
		return (1);
	}
	pthread_mutex_unlock(&philo->_last_meal);
	return (0);
}

int	check_dead(t_philo *philo)
{
	pthread_mutex_lock(&philo->param->_is_dead);
	if (philo->param->is_dead == 1)
	{
		pthread_mutex_unlock(&philo->param->_is_dead);
		return (0);
	}
	pthread_mutex_unlock(&philo->param->_is_dead);
	return (1);
}

int	check_eat(t_philo *philo)
{
	pthread_mutex_lock(&philo->param->_has_eat);
	if (philo->param->has_eat == 1)
	{
		pthread_mutex_unlock(&philo->param->_has_eat);
		return (0);
	}
	pthread_mutex_unlock(&philo->param->_has_eat);
	return (1);
}
