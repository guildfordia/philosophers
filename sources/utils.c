/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 10:28:18 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/09 10:28:20 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philosophers.h"

int	ft_atoi(const char *s)
{
	int	res;
	int	neg;

	neg = 1;
	res = 0;
	while (*s && (*s == ' ' || *s == '\n' || *s == '\t' || *s == '\v'
			|| *s == '\f' || *s == '\r'))
		++s;
	if (*s == '-')
		neg = -1;
	if (*s == '-' || *s == '+')
		++s;
	while (*s && *s >= '0' && *s <= '9')
	{
		res = res * 10 + (*s - 48);
		++s;
	}
	return (res * neg);
}

long	ft_atol(const char *str)
{
	long	nb;
	long	part;
	long	sign;

	if (*str == '-')
		sign = -1;
	else
		sign = 1;
	if (*str == '-' || *str == '+')
		str = str + 1;
	nb = 0;
	while (*str >= '0' && *str <= '9')
		nb = nb * 10 + (*(str++) - '0');
	if (*str == '.' || *str == ',')
	{
		while (*(++str) >= '0' && *str <= '9')
			part = 0;
		while (*(--str) >= '0' && *str <= '9')
			part = (part + (*str - '0')) / 10;
		nb += part;
	}
	return (nb * sign);
}

int	str_digit(const char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
			i++;
		else
			return (0);
	}
	return (1);
}
