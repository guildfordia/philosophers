/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 18:14:57 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/09 18:15:00 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <pthread.h>
# include <sys/time.h>
# include <limits.h>

typedef struct s_param
{
	int				numb;
	long			t_die;
	long			t_eat;
	long			t_sleep;
	int				nb_eat;
	pthread_mutex_t	*fork;
	int				eat_limit;
	int				has_eat;
	pthread_mutex_t	_has_eat;
	int				is_dead;
	pthread_mutex_t	_is_dead;
	pthread_mutex_t	writing;
	pthread_t		_is_alive;
	long			orig_time;
}				t_param;

typedef struct s_philo
{
	pthread_t		tp;
	int				id;
	int				meals;
	pthread_mutex_t	*left;
	pthread_mutex_t	*right;
	t_param			*param;
	pthread_mutex_t	_last_meal;
	long			last_meal;
	struct s_philo	*next;
}				t_philo;

/* philosophers */
void	*create_philo(t_param *param);

/* forks */
int		create_fork(t_param *param);
void	assign_fork(t_param *settings, t_philo *philosopher);

/* tasks */
void	philo_eat(t_philo *philo);
void	philo_sleep(t_philo *philo);
void	philo_think(t_philo *philo);

/* threads */
int		create_thread(t_philo *philo, int nb);
int		join_thread(t_philo *philo);
int		init_mutex(t_philo *philo);

/* parsing */
t_param	*parsing(char **argv);

/* libft */
//int		ft_strcmp(const char *s1, const char *s2);

/* alive check */
void	must_eat(t_philo *philo);
void	is_dead(t_philo *philo);
void	*alive(void *arg);

/* checks */
int		check_eat(t_philo *philo);
int		check_dead(t_philo *philo);
int		check_last_meal(t_philo *philo);

/* time */
void	wait_until(long towait);
long	get_time(void);

/* writing */
void	writing(char *str, t_philo *philo);

/* errors */
int		error_value(t_param *param);
int		error_type(char **argv);
void	wrong_arg(void);

/* utils */
long	ft_atol(const char *str);
int		ft_atoi(const char *str);
int		str_digit(const char *str);

/* exit */
int		exit_philo(t_philo *philo);

/* debug */
void	debug_param(t_param *param);
void	debug_parsing(t_param *param);
void	debug_philo(t_philo *philo);

#endif
